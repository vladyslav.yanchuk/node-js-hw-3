const Joi = require('joi');

module.exports = {
  registerSchema: Joi.object({
    email: Joi.string()
      .required(),
    password: Joi.string()
      .required()
      .min(3),
    role: Joi.string()
      .required()
      .pattern(/^(SHIPPER|DRIVER){1}$/)
  }),

  loginSchema: Joi.object({
    email: Joi.string()
      .required(),
    password: Joi.string()
      .required()
      .min(3)
  }),

  passSchema: Joi.object({
    newPassword: Joi.string()
      .required()
      .min(3),
  }),

  emailSchema: Joi.object({
    email: Joi.string()
      .required(),
  }),
};
