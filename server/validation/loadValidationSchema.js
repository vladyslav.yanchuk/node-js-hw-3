const Joi = require('joi');

module.exports = {
  registerSchema: Joi.object({
    email: Joi.string()
      .required(),
    password: Joi.string()
      .required()
      .min(3)
      .max(30),
    role: Joi.string()
      .required()
      .pattern(/^(SHIPPER|DRIVER){1}$/)
  }),
  truckSchema: Joi.object({
      type: Joi.string()
      .required()
      .pattern(/^(SPRINTER|SMALL STRAIGHT|LARGE STRAIGHT){1}$/),
      created_by: Joi.string()
      .required(),
      created_date: Joi.string()
      .required(),
      status: Joi.string()
      .required()
      .pattern(/^(NEW|POSTED|ASSIGNED|SHIPPED ){1}$/),
      assigned_to: Joi.string()
      .required()
  }),
  typeSchema: Joi.object({
    type: Joi.string()
    .required()
    .pattern(/^(SPRINTER|SMALL STRAIGHT|LARGE STRAIGHT){1}$/)
  }),
  statusSchema: Joi.string()
  .required()
  .pattern(/^(NEW|POSTED|ASSIGNED|SHIPPED ){1}$/),
};
// En route to Pick Up, Arrived to Pick Up, En route to delivery, Arrived to delivery