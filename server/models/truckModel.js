const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
  type: {
    type: String,
    required: true,
  },
  created_by: {
      type: String,
      require: true
  },
  status: {
      type: String,
      required: true
  },
  created_date: {
      type: Date,
      required: true
  },
  assigned_to: {
      type: String,
      required: true
  }
});

module.exports = Truck;
