const mongoose = require('mongoose');

const Load = mongoose.model('Load', {
    name: {
        type: String,
        required: true,
    },
    payload: {
        type: Number,
        require: true
    },
    pickup_address: {
        type: String,
        required: true
    },
    delivery_address: {
        type: String,
        required: true
    },
    created_by: {
        type: String,
        required: true
    },
    assigned_to: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    dimensions: {
        type: Object,
        required: true
    },
    logs: {
        type: Array,
        required: true
    },
    created_date: {
        type: Date,
        required: true
    },
});

module.exports = Load;
