const express = require('express');
const bcrypt = require('bcrypt');
const User = require('../models/userModel');
const {authMiddleware} = require('../middleware/authMiddleware');
const {bcryptPassword} = require('../helpers');
const {passSchema} = require('../validation/authValidationSchemas');
const Load = require('../models/loadModel');

const router = new express.Router();

const returnError = (err, res, code, message) => {
  console.log(err);
  res.status(400).json({message});
};

router.get('/', authMiddleware, async (req, res) => {
  try {
    const { id } = req.data;
    const user = await User.findOne(
        {_id: id},
        {'__v': 0, 'password': 0, 'role': 0}
      );

    res.status(200).json({"user": user});
  } catch ( err ) {
    returnError(err, res, 400, err.message);
  }
});

router.delete('/', authMiddleware, async (req, res) => {
  try {
      const { id, role } = req.data;

      if( role !== 'SHIPPER' ) {
        throw new Error('Only shipper can delete his profile!');
      }

      await User.findOneAndDelete({ _id: id });
      await Load.deleteMany({ created_by: id });

      res.status(200).json({ message: 'Success' });

  } catch (err) {
      console.log(err);
      res.status(400).json({ message: err.message });
  }
});

router.patch('/password', authMiddleware, async (req, res) => {
  try {
    const {email, id} = req.data;
    const {oldPassword, newPassword} = req.body;

    const user = await User.findOne({
      email,
    });

    const {error} = passSchema.validate({newPassword});

    if ( !bcrypt.compareSync(oldPassword, user.password) ) {
      returnError('Wrong password!', res, 400, 'Wrong password!');
    } else if (error) {
      returnError('Wrong password!', res, 400, `Unvalid password!`);
    } else {
      const hash = await bcryptPassword(newPassword);
      User.updateOne({_id: id}, {password: hash}, (err) => {
        if ( err ) {
          returnError(err, res, 400, err.message);
        } else {
          res.status(200).json({message: 'Success'});
        }
      });
    }
  } catch (err) {
    returnError(err, res, 400, err.message);
  }
});

module.exports = router;
