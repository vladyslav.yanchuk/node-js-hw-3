const express = require('express');
const Load = require('../models/loadModel');
const Truck = require('../models/truckModel');
const { authMiddleware } = require('../middleware/authMiddleware');
const { statusSchema } = require('../validation/loadValidationSchema');
const { getTruckCharacteristic, iterateLoadState } = require('../helpers');

const router = new express.Router();

const returnError = (err, res, code, message='Bad request!') => {
    console.log(err);
    res.status(400).json({ message });
};

router.post('/', authMiddleware, async (req, res) => {
    const { id, role } = req.data;
    const { name, payload, pickup_address, delivery_address, dimensions } = req.body;

    if( role !== 'SHIPPER' ) {
        throw new Error('Only shipper can create load!');
    } else {
        const load = new Load({
            name,
            payload,
            pickup_address,
            delivery_address,
            dimensions,
            created_by: id,
            assigned_to: 'null',
            status: 'NEW',
            state: 'NEW',
            logs: [],
            created_date: Date.now()
        });
    
        await load.save((err, results) => {
            if (err) {
                returnError(err, res, 400, 'Server error');
            } else {
                res.status(200).json({ message: 'Load created!' });
            }
        });
    }
});

router.get('/', authMiddleware, async (req, res) => {
    try {
        const { id } = req.data;
        const { status, limit=10, offset=0 } = req.query;

        const search = { created_by: id };
        const options = {
            skip: parseInt(offset),
            limit: limit > 50 ? 10 : parseInt(limit)
        };
        
        if( status ) {
            const {error} = statusSchema.validate(status);
            if( error ) {
                throw new Error(`Unvalid status '${status}'!`);
            }
            search.status = status;
        }

        const loads = await Load.find(search,
            [],
            options
        ).select('-__v');

        res.status(200).json({ loads });
    } catch (err) {
        console.log(err);
        returnError(err, res, 400, err.message);
    }
});

router.get('/active', authMiddleware, async (req, res) => {
    try {
        const { id, role } = req.data;

        if( role !== 'DRIVER' ) {
            throw new Error('Only driver can take load!');
        }

        const driverActiveTruck = await Truck.findOne({ assigned_to: id });

        if( !driverActiveTruck ) {
            throw new Error('You dont have active truck!');
        }

        if( driverActiveTruck.status !== 'IS' ) {
            throw new Error(`Wrong truck status '${driverActiveTruck.status}'!`);
        }

        const { truckSize, truckWeight } = getTruckCharacteristic(driverActiveTruck.type);
        const filter = {
            status: 'POSTED',
            "dimensions.width": { $lte: truckSize.width },
            "dimensions.length": { $lte: truckSize.length },
            "dimensions.height": { $lte: truckSize.height },
            "payload": { $lt: truckWeight }
        };
        const update = {
            logs: [
                {
                    message: `Load assigned to driver with id ${id}`,
                    time: Date.now()
                }
            ],
            assigned_to: id,
            status: "ASSIGNED",
            state: "En route to Pick Up"
        }

        const activeLoad = await Load.findOneAndUpdate(filter, update, { returnOriginal: false });

        if( !activeLoad ) {
            res.json({message: 'Cant find suitable load for your truck!'});
        } else {
            await Truck.findOneAndUpdate({ assigned_to: id }, { status: "OL" });
            res.json({load: activeLoad})
        }
        
    } catch (err) {
        console.log(err);
        returnError(err, res, 400, err.message);
    }
});

router.patch('/active/state', authMiddleware, async (req, res) => {
    try {
        const { id, role } = req.data;

        if( role !== 'DRIVER' ) {
            throw new Error('Only driver can take load!');
        }

        const activeLoad = await Load.findOne({ assigned_to: id });
        const nextState = iterateLoadState(activeLoad.state);
        const update = {
            state: nextState
        };
        
        if( nextState === 'Arrived to delivery' ) {
            update.status = "SHIPPED";
            await Truck.findOneAndUpdate({ assigned_to: id }, { status: 'IS' });
        }

        await activeLoad.update(update);

        res.status(200).json({ 
            message: `Load state changed to '${nextState}'`
        });
        
    } catch (err) {
        console.log(err);
        returnError(err, res, 400, err.message);
    }
});

router.get('/:id', authMiddleware, async (req, res) => {
    try {
        const { id } = req.data;
        const load_id = req.params.id;
        const filter = { _id: load_id, created_by: id };
        const load = await Load.findOne(filter, {__v: 0});

        if( !load ) {
            throw new Error(`No load with id '${load_id}' found!`);
        }
        res.status(200).json({ load });
    } catch (err) {
        console.log(err);
        returnError(err, res, 400, err.message);
    }
});

router.get('/:id/shipping_info', authMiddleware, async (req, res) => {
    try {
        const { id } = req.data;
        const load_id = req.params.id;
        const filter = { _id: load_id, created_by: id };
        const load = await Load.findOne(filter, {__v: 0});

        if( !load ) {
            throw new Error(`No load with id '${load_id}' found!`);
        }

        const truck = Truck.findOne({ id: load.assigned_to });

        res.status(200).json({ load, truck });
    } catch (err) {
        console.log(err);
        returnError(err, res, 400, err.message);
    }
});

router.put('/:id', authMiddleware, async (req, res) => {
    try {
        const { role, id } = req.data;
        const load_id = req.params.id;

        if( role !== 'SHIPPER' ) {
            const message = 'Only shipper can create load!';
            returnError(message, res, 400, message);
        } else {
            await Load.findOneAndUpdate({ _id: load_id, created_by: id }, req.body);
            res.status(200).json({message: "Load details changed successfully"})
        }
    } catch(err) {
        returnError(err, res, 400, res.message);
    }
});

router.post('/:id/post', authMiddleware, async (req, res) => {
    try {
        const { role, id } = req.data;
        const load_id = req.params.id;

        if( role !== 'SHIPPER' ) {
            const message = 'Only shipper can post load!';
            returnError(message, res, 400, message);
        } else {
            const loadToPost = await Load.findOne({ _id: load_id, created_by: id });

            if( !loadToPost ) {
                throw new Error(`Load with id '${id}' is not exist!`);
            }

            await loadToPost.update({ status: 'POSTED' });

            res.status(200).json({
                message: "Load successfully posted!",
                driver_found: true
            })
        }
    } catch(err) {
        returnError(err, res, 400, res.message);
    }
});

router.delete('/:id', authMiddleware, async (req, res) => {
    try {
        const { role, id } = req.data;
        const load_id = req.params.id;

        if( role !== 'SHIPPER' ) {
            throw new Error('Only shipper can delete load!')
        } 

        const loadToDelete = await Load.findOne({ _id: load_id, created_by: id});

        if( loadToDelete.status !== 'NEW' ) {
            throw new Error(`You cant delete ${loadToDelete.status} load!`);
        }

        await loadToDelete.delete();
        
        res.status(200).json({message: "Load successfully deleted!"})

    } catch(err) {
        returnError(err, res, 400, res.message);
    }
});

router.get('/:id', authMiddleware, async (req, res) => {
    try {
        const { id } = req.data;
        const load_id = req.params.id;
        const filter = { _id: load_id, created_by: id };
        const load = await Load.findOne(filter, {__v: 0});

        if( !load ) {
            throw new Error(`No load with id '${load_id}' found!`);
        }
        res.status(200).json({ load });
    } catch (err) {
        console.log(err);
        returnError(err, res, 400, err.message);
    }
});


router.post('/:id/assign', authMiddleware, async (req, res) => {
    try {
        const { id } = req.data;
        const truck_id = req.params.id;
        const filter = { _id: truck_id, created_by: id };
        const update = { assigned_to: id };
        await Truck.findByIdAndUpdate(filter, update);

        res.status(200).json({ message: 'Success' });
    } catch (err) {
        console.log(err);
        returnError(err, res, 400, err.message);
    }
});

router.delete('/:id', authMiddleware, async (req, res) => {
    try {
        const { id } = req.data;
        const truck_id = req.params.id;
        const filter = { _id: truck_id, created_by: id };
        await Truck.findByIdAndDelete(filter);

        res.status(200).json({ message: 'Success' });
    } catch (err) {
        console.log(err);
        returnError(err, res, 400, err.message);
    }
});

module.exports = router;
