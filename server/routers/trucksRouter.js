const express = require('express');
const Truck = require('../models/truckModel');
const { authMiddleware } = require('../middleware/authMiddleware');
const { truckSchema, typeSchema } = require('../validation/truckValidationSchemas');

const router = new express.Router();

const returnServerError = message => {
    res.status(500).json({ message: message ? message : 'Server error!' });
};

router.post('/', authMiddleware, async (req, res) => {
    try {
        const { id, role } = req.data;
        const { type } = req.body;
        
        if( role !== 'DRIVER' ) {
            throw new Error('Only driver can create truck!');
        }
    
        const truck = new Truck({
            type,
            created_by: id,
            status: 'IS',
            created_date: Date.now(),
            assigned_to: 'null'
        });
    
        await truck.save((err, results) => {
            if (err) {
                returnServerError(err.message);
            } else {
                res.status(200).json({ message: 'Truck created!' });
            }
        });
    } catch(err) {
        res.status(400).json({ message: err.message ? err.message : 'Client error' });
    }
});

router.get('/', authMiddleware, async (req, res) => {
    try {
        const { id } = req.data;
        const trucks = await Truck.find({
            created_by: id,
        }, { '__v': 0 });

        res.status(200).json({ trucks });

    } catch (err) {
        console.log(err);
        res.status(400).json({ message: err.message ? err.message : 'Client error' });
    }
});

router.get('/:id', authMiddleware, async (req, res) => {
    try {
        const { id } = req.data;
        const truck_id = req.params.id;
        const filter = { _id: truck_id, created_by: id };
        const truck = await Truck.findOne(filter, {__v: 0});

        if( !truck ) {
            throw new Error(`No truck with id '${truck_id}' found!`);
        }

        res.status(200).json({ truck });

    } catch (err) {
        console.log(err);
        res.status(400).json({ message: err.message ? err.message : 'Client error' });
    }
});

router.post('/:id/assign', authMiddleware, async (req, res) => {
    try {
        const { id } = req.data;
        const truck_id = req.params.id;
        const filter = { _id: truck_id, created_by: id };
        const update = { assigned_to: id };

        // unassign current truck 
        await Truck.findOneAndUpdate({ assigned_to: id }, { assigned_to: null });
        
        // assign new one
        await Truck.findByIdAndUpdate(filter, update);

        res.status(200).json({ message: 'Success' });
    } catch (err) {
        console.log(err);
        res.status(400).json({ message: err.message ? err.message : 'Client error' });
    }
});

router.put('/:id', authMiddleware, async (req, res) => {
    try {
        const { id } = req.data;
        const truck_id = req.params.id;
        const newType = req.body.type;
        const {error} = typeSchema.validate({type: newType});

        if( error ) {
            throw new Error('Unvalid truck type!');
        }

        const filter = { _id: truck_id, created_by: id };
        const update = { type: newType };

        const truck = await Truck.findOne(filter);

        if( truck.assigned_to ) {
            throw new Error('You cant update assigned truck info!');
        }

        await truck.update(update);

        res.status(200).json({ message: 'Success' });
        
    } catch (err) {
        console.log(err);
        res.status(400).json({ message: err.message ? err.message : 'Client error' });
    }
});

router.delete('/:id', authMiddleware, async (req, res) => {
    try {
        const { id } = req.data;
        const truck_id = req.params.id;
        const filter = { _id: truck_id, created_by: id };

        const truck = await Truck.findOne(filter);

        if( truck.assigned_to ) {
            throw new Error('You cant delete assigned truck!');
        }

        await truck.deleteOne();

        res.status(200).json({ message: 'Success' });
    } catch (err) {
        console.log(err);
        res.status(400).json({ message: err.message ? err.message : 'Client error' });
    }
});

module.exports = router;
