const express = require('express');
const jwt = require('jsonwebtoken');
const { registerSchema, loginSchema, emailSchema } = require('../validation/authValidationSchemas');
const bcrypt = require('bcrypt');
const User = require('../models/userModel');
const { JWT_SECRET } = require('../config');
const { bcryptPassword } = require('../helpers');

const router = new express.Router();

router.post('/register', async (req, res) => {
  try {
    const { email, password, role } = req.body;
    const { error } = registerSchema.validate({ email, password, role });

    if (error) {
      throw new Error('Invalid fields!');
    }

    const user = new User({
      email,
      password: await bcryptPassword(password),
      createdDate: Date.now(),
      role
    });

    await user.save((err, results) => {
      if (err) {
        throw new Error('Server error!');
      }

      res.status(200).json({ message: 'Successfull registration!' });
    });

  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message ? err.message : 'Client error' });
  }
});

router.post('/login', async (req, res) => {
  try {
    const { email, password } = req.body;
    const { error } = loginSchema.validate({ email, password });

    if (error) {
      throw new Error('Invalid fields!');
    }

    const user = await User.findOne({ email });

    if (!user) {
      throw new Error(`No user with email '${email}' found!`)
    }

    if (!bcrypt.compareSync(password, user.password)) {
      throw new Error('Wrong password!');
    }

    const token = jwt.sign({ email: user.email, id: user.id, role: user.role },
      JWT_SECRET);

    res.status(200).json({ jwt_token: token });

  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message ? err.message : 'Client error' });
  }
});

router.post('/forgot_password', async (req, res) => {
  try {
    const { email } = req.body;
    const { error } = emailSchema.validate({ email });

    if (error) {
      throw new Error('Invalid fields');
    }
    const user = await User.findOne({ email });

    if (!user) {
      throw new Error(`No user with email '${email}' found!`)
    }

    res.status(200).json({ message: "New password sent to your email address" });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message ? err.message : 'Client error' });
  }
});

module.exports = router;
