const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');
const {PORT} = require('./config');
const authRouter = require('./routers/authRouter');
const trucksRouter = require('./routers/trucksRouter');
const loadsRouter = require('./routers/loadsRouter');
const usersRouter = require('./routers/usersRouter');

const app = express();

mongoose.connect(`mongodb+srv://test-user-1:zxc123@cluster0.mvh0n.mongodb.net/node-js-hw-3?retryWrites=true&w=majority`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
});

app.use(express.json());
app.use(morgan('tiny'));
app.use(cors());
app.use('/api/auth', authRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);
app.use('/api/users/me', usersRouter);

app.listen(PORT, () => {
  console.log(`Server works at port ${PORT}!`);
});
