const bcrypt = require('bcrypt');
const { truckSchema } = require('./validation/loadValidationSchema');
module.exports = {
  bcryptPassword: async (password) => {
    const saltRounds = 10;
    const hashedPassword = await new Promise((resolve, reject) => {
      bcrypt.hash(password, saltRounds, (err, hash) => {
        if (err) reject(err);
        resolve(hash);
      });
    });
    return hashedPassword;
  },
  getTruckCharacteristic: (truckType, loadSize, loadWeight) => {
    let truckSize, truckWeight;
    switch (truckType) {
      case 'SPRINTER':
        truckSize = {
          width: 300,
          length: 250,
          height: 170
        }
        truckWeight = 1700;
        break;
      case 'SMALL STRAIGHT':
        truckSize = {
          width: 500,
          length: 250,
          height: 170
        }
        truckWeight = 2500;
        break;
      case 'LARGE STRAIGHT':
        truckSize = {
          width: 700,
          length: 350,
          height: 200
        }
        truckWeight = 4000;
        break;
    };

    return { truckSize, truckWeight };
  },
  iterateLoadState: curState => {
    const states = [ "En route to Pick Up", "Arrived to Pick Up", "En route to delivery", "Arrived to delivery" ];
    const index = states.findIndex(el => el === curState);
    return index !== states.length - 1 ? states[index + 1] : states[index]; 
  } 
};
